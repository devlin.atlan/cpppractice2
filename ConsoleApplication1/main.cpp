#include <iostream>
#include <string>
#include "Helpers.h"

// �������� ������� 13
void RunHomework13()
{
	int a = 0, b = 0;
	std::cout << "Type int A\n";
	std::cin >> a;
	std::cout << "Type int B\n";
	std::cin >> b;
	std::cout << "\n" << "Square of (A + B) = " << sumInt(a, b) << "\n";
}
// �������� ������� 14
void RunHomework14()
{
	std::string moo = "Mooooo!";
	std::cout << "String is: " + moo << "\n";
	std::cout << "String length is: " << moo.length() << "\n";
	std::cout << "First symbol is: " << moo[0] << "\n";
	std::cout << "Last symbol is: " << moo[moo.length() - 1] << "\n";
}
// �������, ��������� �� ����� ������ ������ ����� �� ���������� ��������. 
// ���� ������ �������� == true, �� ��������� ������ �����. ���� == false, �� ��������.
void PrintEvenOnly(int N, bool even = true)
{
	std::cout << "\n\nYour numbers, sir: \n";
	for (int i = 0; i <= N; ++i)
	{
		switch ((i+ even) % 2)
		{
		case 1:
			std::cout << i << "\n";
			break;
		case 0:
			break;
		default:
			break;
		}
	}
}
// �������� ������� 15
void RunHomework15(int N)
{
	bool PrintEven = true;
	std::cout << "To print only even integer numbers from 0 to " << N << " write 1,\n to print not even numbers write 0\n";
	std::cin >> PrintEven;
	PrintEvenOnly(N, PrintEven);
}

int main()
{
	const int N = 15;
	int HomeworkNumber = 0;
	std::cout << "Set homework number (13, 14, 15). Write otherwise to exit.\n";
	std::cin >> HomeworkNumber;
	if (HomeworkNumber == 13)
	{
		RunHomework13();
	}
	else if (HomeworkNumber == 14)
	{
		RunHomework14();
	}
	else if (HomeworkNumber == 15)
	{
		RunHomework15(N);
	}
	system("pause");
	return 0;
}